// une entreprise peut avoir plusieurs produits à vendre
class Entreprise {
    constructor(cash, stock) {
        // toujours sup à 100
        this.cash = cash;
        this.stock = stock;
    }
    //fonction achat =  Prix x Qté => Augmentation du stock + Diminution du cash
    buy(price, quantity, name) {
        let total = price * quantity
        if (this.cash - total >= 100) {
            // += corespond à  stock= stock + quantity
            this.stock += quantity;
            // -= corespond à  cash = cash -total
            this.cash -= total
        }
    }
    //fonction vente = Prix x Qté => Augmentation du cash + Diminution du stock
    sell(price, quantity, name) {
        if (this.cash - total >= 100) {
            // += corespond à  stock= stock + quantity
            this.stock += quantity;
            // -= corespond à  cash = cash -total
            this.cash -= total

        }
    }
}
// un produit peut contenir plusieurs produits

class Product {
    constructor(name, sellprice, currency) {
        this.nomdeproduit = name;
        // jamais inférieur à 0
        this.prix = sellprice;
        this.currency = currency
    }
}

class Currency {
    static euro;
    static dollar;
    static yen
}

const VeloEurope = new Entreprise();
const Velovapeur = new Product('Velo Tagazou', 1580, 'euro')

console.log(Velovapeur)

export default Entreprise;